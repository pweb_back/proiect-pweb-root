# Proiect PWeb

Ştefan Silviu-Alexandru (342C1, PWeb)  
Calancea Corneliu (344C5, PWeb + IDP)  
Ciobanu Dorin (342C3, IDP)  

- Figma: https://www.figma.com/file/yii2Xl7epYLgzBqE4HelqP/Proiect-Pweb?node-id=4%3A0
- Frontend repo: https://github.com/slak44/proiect-pweb
- Macro architecture : https://drive.google.com/file/d/1fBS1rNsYJgtyL-9sevkbvkujQqUUjn1N/view?usp=sharing
- Data architecture : https://drive.google.com/file/d/12e4XPgfkCKOsDM1Oyr_pB_gsaKDcjTK_/view?usp=sharing
- IO service repo: https://gitlab.com/pweb_back/io_microservice
- Users service repo: https://gitlab.com/pweb_back/users_microservice
- Posts service repo: https://gitlab.com/pweb_back/posts_microservice

### User stories

Pweb
- As a designer, I’d like to have a set of wireframes that will serve as base for a prototype
- As a developer, I’d like to have a prototype of the platform in order to start developing the front end
- As a developer, I’d like to have a well-defined architecture before proceeding to work
- As an administrator, I’d like to see all user accounts
- As an administrator, I’d like to edit user accounts
- As an administrator, I’d like to delete user accounts
- As an administrator, I’d like to edit help requests
- As an administrator, I’d like to delete help requests
- As an administrator, I’d like to be able to mark an account as verified
- As a user, I’d like to be able to create an account on the platform
- As a user, I’d like to be able to edit my account details on the platform
- As a user, I’d like to be able to log into my account on the platform
- As a user, I’d like to be able to log out off my account on the platform
- As a user, I’d like to post a request for help
- As a user, I’d like to help a user who posted a request
- As a user, I’d like to post an offer to help war refugees
- As a user, I’d like to accept a help offer from another user
- As a user, I’d like to edit an announcement that I’ve made earlier
- As a user, I’d like to tag my announcement according to the topic (category)
- As a user, I’d like to retire/disable my post
- As a user, I’d like to permanently delete my own post
- As a user, I’d like to upvote/downvote the best posts

IDP
- As a developer, I’d like to have my commits automatically tested and deployed
- As a developer, I’d like to have the platform routes defined so that I don’t have to define them in the back end
- As a developer, I’d like our platform deployed in Docker so that it can be easily run and managed
- As a DevOps engineer, I’d like to have a logging system with a dashboard, so that I can monitor the platform usage
